﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APCSystem.Models;

namespace APCSystem.Controllers
{
    public class PayrollItemsController : Controller
    {
        private PaediatricDBEntities db = new PaediatricDBEntities();

        // GET: PayrollItems
        public ActionResult Index()
        {
            return View(db.PayrollItems.ToList());
        }

        // GET: PayrollItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollItem payrollItem = db.PayrollItems.Find(id);
            if (payrollItem == null)
            {
                return HttpNotFound();
            }
            return View(payrollItem);
        }

        // GET: PayrollItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PLI_ID,PLI_Name")] PayrollItem payrollItem)
        {
            if (ModelState.IsValid)
            {
                db.PayrollItems.Add(payrollItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollItem);
        }

        // GET: PayrollItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollItem payrollItem = db.PayrollItems.Find(id);
            if (payrollItem == null)
            {
                return HttpNotFound();
            }
            return View(payrollItem);
        }

        // POST: PayrollItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PLI_ID,PLI_Name")] PayrollItem payrollItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payrollItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollItem);
        }

        // GET: PayrollItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollItem payrollItem = db.PayrollItems.Find(id);
            if (payrollItem == null)
            {
                return HttpNotFound();
            }
            return View(payrollItem);
        }

        // POST: PayrollItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollItem payrollItem = db.PayrollItems.Find(id);
            db.PayrollItems.Remove(payrollItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
