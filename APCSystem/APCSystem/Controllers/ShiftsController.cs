﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APCSystem.Models;

namespace APCSystem.Controllers
{
    public class ShiftsController : Controller
    {
        private PaediatricDBEntities db = new PaediatricDBEntities();

        // GET: Shifts
        public ActionResult Index()
        {
            var shifts = db.Shifts.Include(s => s.Branch).Include(s => s.ShiftType);
            return View(shifts.ToList());
        }

        // GET: Shifts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shift shift = db.Shifts.Find(id);
            if (shift == null)
            {
                return HttpNotFound();
            }
            return View(shift);
        }

        // GET: Shifts/Create
        public ActionResult Create()
        {
            ViewBag.SFT_BCH_ID = new SelectList(db.Branches, "BCH_ID", "BCH_Name");
            ViewBag.SFT_SFY_ID = new SelectList(db.ShiftTypes, "SFY_ID", "SFY_Name");
            if (db.Shifts.ToList().Count() < 1)
            {
                return View("CreateEmpty");
            }
            else
            {
                return View(db.Shifts.ToList());
            }
        }

        // POST: Shifts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SFT_ID,SFT_Name,SFT_BCH_ID,SFT_StartDuration,SFT_EndDuration,SFT_SFY_ID")] Shift shift)
        {
            if (ModelState.IsValid)
            {
                db.Shifts.Add(shift);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            ViewBag.SFT_BCH_ID = new SelectList(db.Branches, "BCH_ID", "BCH_Name", shift.SFT_BCH_ID);
            ViewBag.SFT_SFY_ID = new SelectList(db.ShiftTypes, "SFY_ID", "SFY_Name", shift.SFT_SFY_ID);
            return View(shift);
        }

        // GET: Shifts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shift shift = db.Shifts.Find(id);
            if (shift == null)
            {
                return HttpNotFound();
            }
            ViewBag.SFT_BCH_ID = new SelectList(db.Branches, "BCH_ID", "BCH_Name", shift.SFT_BCH_ID);
            ViewBag.SFT_SFY_ID = new SelectList(db.ShiftTypes, "SFY_ID", "SFY_Name", shift.SFT_SFY_ID);
            return View(shift);
        }

        // POST: Shifts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SFT_ID,SFT_Name,SFT_BCH_ID,SFT_StartDuration,SFT_EndDuration,SFT_SFY_ID")] Shift shift)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shift).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SFT_BCH_ID = new SelectList(db.Branches, "BCH_ID", "BCH_Name", shift.SFT_BCH_ID);
            ViewBag.SFT_SFY_ID = new SelectList(db.ShiftTypes, "SFY_ID", "SFY_Name", shift.SFT_SFY_ID);
            return View(shift);
        }

        // GET: Shifts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shift shift = db.Shifts.Find(id);
            if (shift == null)
            {
                return HttpNotFound();
            }
            return View(shift);
        }

        // POST: Shifts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Shift shift = db.Shifts.Find(id);
            db.Shifts.Remove(shift);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
