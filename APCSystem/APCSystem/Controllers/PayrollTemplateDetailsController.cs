﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APCSystem.Models;
using APCSystem.ViewModels;

namespace APCSystem.Controllers
{
    public class PayrollTemplateDetailsController : Controller
    {
        // GET: PayrollTemplateDetails
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            PaediatricDBEntities db = new PaediatricDBEntities();
            List<PayrollVM> payrollVMs = new List<PayrollVM>();
            var payrollTemplate = from paytem in db.PayrollTemplates
                                  join payitem in db.PayrollItems
                                  on paytem.PLT_ID equals payitem.PLI_ID
                                  select new { paytem.PLT_Name, paytem.PLT_BasicSalary, paytem.PLT_GrossSalary, paytem.PLT_NetSalary, payitem.PLI_Name };
            return View();
        }
    }
}