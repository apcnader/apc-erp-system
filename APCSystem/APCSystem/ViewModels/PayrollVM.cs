﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APCSystem.ViewModels
{
    public class PayrollVM
    {
        public string PLT_Name { get; set; } //PayrollTemplate
        public Nullable<decimal> PLT_BasicSalary { get; set; } //PayrollTemplate
        public Nullable<decimal> PLT_GrossSalary { get; set; } //PayrollTemplate
        public Nullable<decimal> PLT_NetSalary { get; set; } //PayrollTemplate
        public int PLI_ID { get; set; } //PayrollItem
        public Nullable<bool> PTD_Action { get; set; } //PayrollTemplateDetails
        public Nullable<decimal> PTD_Value { get; set; } //PayrollTemplateDetails
    }
}